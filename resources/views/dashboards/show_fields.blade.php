<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', 'Name:') !!}
    <p>{{ $dashboard->name }}</p>
</div>

<!-- Status Field -->
<div class="form-group">
    {!! Form::label('Status', 'Status:') !!}
    <p>{{ $dashboard->Status }}</p>
</div>

<!-- Details Field -->
<div class="form-group">
    {!! Form::label('Details', 'Details:') !!}
    <p>{{ $dashboard->Details }}</p>
</div>

