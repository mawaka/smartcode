<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\Dashboard;

class DashboardApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_dashboard()
    {
        $dashboard = factory(Dashboard::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/dashboards', $dashboard
        );

        $this->assertApiResponse($dashboard);
    }

    /**
     * @test
     */
    public function test_read_dashboard()
    {
        $dashboard = factory(Dashboard::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/dashboards/'.$dashboard->id
        );

        $this->assertApiResponse($dashboard->toArray());
    }

    /**
     * @test
     */
    public function test_update_dashboard()
    {
        $dashboard = factory(Dashboard::class)->create();
        $editedDashboard = factory(Dashboard::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/dashboards/'.$dashboard->id,
            $editedDashboard
        );

        $this->assertApiResponse($editedDashboard);
    }

    /**
     * @test
     */
    public function test_delete_dashboard()
    {
        $dashboard = factory(Dashboard::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/dashboards/'.$dashboard->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/dashboards/'.$dashboard->id
        );

        $this->response->assertStatus(404);
    }
}
