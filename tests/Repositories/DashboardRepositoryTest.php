<?php namespace Tests\Repositories;

use App\Models\Dashboard;
use App\Repositories\DashboardRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class DashboardRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var DashboardRepository
     */
    protected $dashboardRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->dashboardRepo = \App::make(DashboardRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_dashboard()
    {
        $dashboard = factory(Dashboard::class)->make()->toArray();

        $createdDashboard = $this->dashboardRepo->create($dashboard);

        $createdDashboard = $createdDashboard->toArray();
        $this->assertArrayHasKey('id', $createdDashboard);
        $this->assertNotNull($createdDashboard['id'], 'Created Dashboard must have id specified');
        $this->assertNotNull(Dashboard::find($createdDashboard['id']), 'Dashboard with given id must be in DB');
        $this->assertModelData($dashboard, $createdDashboard);
    }

    /**
     * @test read
     */
    public function test_read_dashboard()
    {
        $dashboard = factory(Dashboard::class)->create();

        $dbDashboard = $this->dashboardRepo->find($dashboard->id);

        $dbDashboard = $dbDashboard->toArray();
        $this->assertModelData($dashboard->toArray(), $dbDashboard);
    }

    /**
     * @test update
     */
    public function test_update_dashboard()
    {
        $dashboard = factory(Dashboard::class)->create();
        $fakeDashboard = factory(Dashboard::class)->make()->toArray();

        $updatedDashboard = $this->dashboardRepo->update($fakeDashboard, $dashboard->id);

        $this->assertModelData($fakeDashboard, $updatedDashboard->toArray());
        $dbDashboard = $this->dashboardRepo->find($dashboard->id);
        $this->assertModelData($fakeDashboard, $dbDashboard->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_dashboard()
    {
        $dashboard = factory(Dashboard::class)->create();

        $resp = $this->dashboardRepo->delete($dashboard->id);

        $this->assertTrue($resp);
        $this->assertNull(Dashboard::find($dashboard->id), 'Dashboard should not exist in DB');
    }
}
