<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Product;
use Faker\Generator as Faker;

$factory->define(Product::class, function (Faker $faker) {

    return [
        'name' => $faker->word,
        'price' => $faker->word,
        'dprice' => $faker->word,
        'location' => $faker->word,
        'size' => $faker->word,
        'description' => $faker->text,
        'region' => $faker->word,
        'product' => $faker->word,
        'quantity' => $faker->randomDigitNotNull,
        'vendor' => $faker->randomDigitNotNull,
        'status' => $faker->randomDigitNotNull,
        'color' => $faker->word,
        'categoryid' => $faker->word,
        'views' => $faker->randomDigitNotNull,
        'rate' => $faker->randomDigitNotNull,
        'like' => $faker->word,
        'availability' => $faker->word,
        'phone' => $faker->word,
        'font_image' => $faker->word,
        'image1' => $faker->word,
        'image2' => $faker->word,
        'image3' => $faker->word,
        'image4' => $faker->word,
        'image5' => $faker->word,
        'image6' => $faker->word,
        'image7' => $faker->word,
        'image8' => $faker->word,
        'image9' => $faker->word,
        'image10' => $faker->word,
        'image11' => $faker->word,
        'image12' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'deleted_at' => $faker->date('Y-m-d H:i:s')
    ];
});
