<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Dashboard;
use Faker\Generator as Faker;

$factory->define(Dashboard::class, function (Faker $faker) {

    return [
        'name' => $faker->word,
        'Status' => $faker->word,
        'Details' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'deleted_at' => $faker->date('Y-m-d H:i:s')
    ];
});
