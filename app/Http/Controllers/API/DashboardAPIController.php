<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateDashboardAPIRequest;
use App\Http\Requests\API\UpdateDashboardAPIRequest;
use App\Models\Dashboard;
use App\Repositories\DashboardRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class DashboardController
 * @package App\Http\Controllers\API
 */

class DashboardAPIController extends AppBaseController
{
    /** @var  DashboardRepository */
    private $dashboardRepository;

    public function __construct(DashboardRepository $dashboardRepo)
    {
        $this->dashboardRepository = $dashboardRepo;
    }

    /**
     * Display a listing of the Dashboard.
     * GET|HEAD /dashboards
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $dashboards = $this->dashboardRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($dashboards->toArray(), 'Dashboards retrieved successfully');
    }

    /**
     * Store a newly created Dashboard in storage.
     * POST /dashboards
     *
     * @param CreateDashboardAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateDashboardAPIRequest $request)
    {
        $input = $request->all();

        $dashboard = $this->dashboardRepository->create($input);

        return $this->sendResponse($dashboard->toArray(), 'Dashboard saved successfully');
    }

    /**
     * Display the specified Dashboard.
     * GET|HEAD /dashboards/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Dashboard $dashboard */
        $dashboard = $this->dashboardRepository->find($id);

        if (empty($dashboard)) {
            return $this->sendError('Dashboard not found');
        }

        return $this->sendResponse($dashboard->toArray(), 'Dashboard retrieved successfully');
    }

    /**
     * Update the specified Dashboard in storage.
     * PUT/PATCH /dashboards/{id}
     *
     * @param int $id
     * @param UpdateDashboardAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateDashboardAPIRequest $request)
    {
        $input = $request->all();

        /** @var Dashboard $dashboard */
        $dashboard = $this->dashboardRepository->find($id);

        if (empty($dashboard)) {
            return $this->sendError('Dashboard not found');
        }

        $dashboard = $this->dashboardRepository->update($input, $id);

        return $this->sendResponse($dashboard->toArray(), 'Dashboard updated successfully');
    }

    /**
     * Remove the specified Dashboard from storage.
     * DELETE /dashboards/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Dashboard $dashboard */
        $dashboard = $this->dashboardRepository->find($id);

        if (empty($dashboard)) {
            return $this->sendError('Dashboard not found');
        }

        $dashboard->delete();

        return $this->sendSuccess('Dashboard deleted successfully');
    }
}
