<?php

namespace App\Http\Controllers;

use App\DataTables\DashboardDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateDashboardRequest;
use App\Http\Requests\UpdateDashboardRequest;
use App\Repositories\DashboardRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class DashboardController extends AppBaseController
{
    /** @var  DashboardRepository */
    private $dashboardRepository;

    public function __construct(DashboardRepository $dashboardRepo)
    {
        $this->dashboardRepository = $dashboardRepo;
    }

    /**
     * Display a listing of the Dashboard.
     *
     * @param DashboardDataTable $dashboardDataTable
     * @return Response
     */
    public function index(DashboardDataTable $dashboardDataTable)
    {
        return $dashboardDataTable->render('dashboards.index');
    }

    /**
     * Show the form for creating a new Dashboard.
     *
     * @return Response
     */
    public function create()
    {
        return view('dashboards.create');
    }

    /**
     * Store a newly created Dashboard in storage.
     *
     * @param CreateDashboardRequest $request
     *
     * @return Response
     */
    public function store(CreateDashboardRequest $request)
    {
        $input = $request->all();

        $dashboard = $this->dashboardRepository->create($input);

        Flash::success('Dashboard saved successfully.');

        return redirect(route('dashboards.index'));
    }

    /**
     * Display the specified Dashboard.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $dashboard = $this->dashboardRepository->find($id);

        if (empty($dashboard)) {
            Flash::error('Dashboard not found');

            return redirect(route('dashboards.index'));
        }

        return view('dashboards.show')->with('dashboard', $dashboard);
    }

    /**
     * Show the form for editing the specified Dashboard.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $dashboard = $this->dashboardRepository->find($id);

        if (empty($dashboard)) {
            Flash::error('Dashboard not found');

            return redirect(route('dashboards.index'));
        }

        return view('dashboards.edit')->with('dashboard', $dashboard);
    }

    /**
     * Update the specified Dashboard in storage.
     *
     * @param  int              $id
     * @param UpdateDashboardRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateDashboardRequest $request)
    {
        $dashboard = $this->dashboardRepository->find($id);

        if (empty($dashboard)) {
            Flash::error('Dashboard not found');

            return redirect(route('dashboards.index'));
        }

        $dashboard = $this->dashboardRepository->update($request->all(), $id);

        Flash::success('Dashboard updated successfully.');

        return redirect(route('dashboards.index'));
    }

    /**
     * Remove the specified Dashboard from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $dashboard = $this->dashboardRepository->find($id);

        if (empty($dashboard)) {
            Flash::error('Dashboard not found');

            return redirect(route('dashboards.index'));
        }

        $this->dashboardRepository->delete($id);

        Flash::success('Dashboard deleted successfully.');

        return redirect(route('dashboards.index'));
    }
}
