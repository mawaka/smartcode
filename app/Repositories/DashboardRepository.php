<?php

namespace App\Repositories;

use App\Models\Dashboard;
use App\Repositories\BaseRepository;

/**
 * Class DashboardRepository
 * @package App\Repositories
 * @version March 6, 2020, 1:31 pm UTC
*/

class DashboardRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'Status',
        'Details'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Dashboard::class;
    }
}
