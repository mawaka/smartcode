<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Dashboard
 * @package App\Models
 * @version March 6, 2020, 1:31 pm UTC
 *
 * @property string name
 * @property string Status
 * @property string Details
 */
class Dashboard extends Model
{
    use SoftDeletes;

    public $table = 'dashboards';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'name',
        'Status',
        'Details'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'Status' => 'string',
        'Details' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required',
        'Status' => 'required',
        'Details' => 'required'
    ];

    
}
